import 'package:bks_gold/services/firebase_services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DashBoard extends StatefulWidget {
  UserCredential user;
    DashBoard({ required this.user, Key? key}) : super(key: key);

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {

  final TextEditingController _textFieldController = TextEditingController();
  String selected = "";
   List<dynamic>? tasks = [];
  var firebaseUser, userId;

  List checkListItems = [
    {
      "id": 0,
      "value": false,
      "title": "Mark as Complete",
    },
    {
      "id": 1,
      "value": false,
      "title": "Incomplete",
    },
  ];
  final fSInstance = FirebaseFirestore.instance;
  final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance.collection('users').snapshots();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseUser =  FirebaseAuth.instance.currentUser;
    userId = firebaseUser?.uid;
    }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        leadingWidth: 30,
        title: Row(
          children: [
            CircleAvatar(
              radius: 20,
              backgroundImage:NetworkImage(
                "${widget.user.user?.photoURL}",
              ),
            ),
            SizedBox(width: 10,),
            Text("${widget.user.user?.displayName}")
          ],
        ),
        actions: [
          TextButton(onPressed: (){
            FirebaseService().logout();
            Navigator.pop(context);
          },
              child: const Text("Logout",style: TextStyle(color: Colors.white),))
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance.collection('users').doc(userId).collection('tasks').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text("Loading");
        }
        final tasks = snapshot.data!.docs;
        print("Task $tasks");
        if(tasks.isEmpty){
          return const Center(child: Text("No Data Listed"),);
        }else{
          return ListView.separated(itemBuilder: (BuildContext context, int index) {
            return Card(
              child: Padding(
                padding: const EdgeInsets.only(left: 10,top: 10,bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("${tasks[index]['desc']}"),),
                          IconButton(
                              onPressed:(){
                                var itemId = tasks[index].id;
                                FirebaseFirestore.instance
                                    .collection('users') // Replace with your outer collection
                                    .doc(userId)
                                    .collection('tasks') // Replace with the inner collection
                                    .doc(itemId)
                                    .delete()
                                    .then((_) {
                                  print('Item added successfully');
                                }).catchError((error) {
                                  print('Error adding new item: $error');
                                });
                              },
                              icon: const Icon(Icons.delete,color: Colors.red,))
                        ],
                      ),
                      onTap: (){
                        var description = tasks[index]['desc'];
                        var status = tasks[index]['status'];
                        var itemId = tasks[index].id;
                        print("Id $itemId");
                        _displayEditInputDialog(context, description, itemId, status);
                      },
                    ),
                    Column(
                      children: [
                        InkWell(
                          child: Row(
                            children:  [
                              tasks[index]['status'] != "Mark as Completed"
                                  ? const Icon(Icons.check_box_outline_blank)
                                  : const Icon(Icons.check_box,color: Colors.blue,),
                              const SizedBox(width: 10,),
                              const Text("Mark as Completed")
                            ],
                          ),
                          onTap: (){
                            var userDocumentId = userId;
                            var itemDocumentId = tasks[index].id;// Replace with the document ID
                            final updatedData = {
                              'status': "Mark as Completed",
                              'desc': tasks[index]['desc'],
                            };
                            FirebaseFirestore.instance
                                .collection('users') // Replace with your outer collection
                                .doc(userDocumentId)
                                .collection('tasks') // Replace with the inner collection
                                .doc(itemDocumentId)
                                .update(updatedData)
                                .then((_) {
                              print('New item added successfully');
                            }).catchError((error) {
                              print('Error adding new item: $error');
                            });
                          },
                        ),
                        const SizedBox(height: 10,),
                        InkWell(
                          child: Row(
                            children:  [
                              tasks[index]['status'] != "Incomplete"
                                  ? const Icon(Icons.check_box_outline_blank)
                                  : const Icon(Icons.check_box,color: Colors.blue,),
                              const SizedBox(width: 10,),
                              const Text("Incomplete")
                            ],
                          ),
                          onTap: (){
                            var userDocumentId = userId;
                            var itemDocumentId = tasks[index].id;// Replace with the document ID
                            final updatedData = {
                              'status': "Incomplete",
                              'desc': tasks[index]['desc'],
                            };
                            FirebaseFirestore.instance
                                .collection('users') // Replace with your outer collection
                                .doc(userDocumentId)
                                .collection('tasks') // Replace with the inner collection
                                .doc(itemDocumentId)
                                .update(updatedData)
                                .then((_) {
                              print('New item added successfully');
                            }).catchError((error) {
                              print('Error adding new item: $error');
                            });
                          },
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
            separatorBuilder: (BuildContext context, int index) {
              return const SizedBox(height: 10,);
            },
            itemCount: tasks.length,
          );
        }

      },
    ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _displayTextInputDialog(context);
        },
        child: const Icon(Icons.add),

      ),
    );
  }

  String? codeDialog;
  String? valueText;

  Future<void> _displayTextInputDialog(BuildContext context) async {
    _textFieldController.clear();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Task Description'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  valueText = value;
                });
              },
              controller: _textFieldController,
              decoration: const InputDecoration(hintText: "Enter task description"),
            ),
            actions: <Widget>[
              MaterialButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text('OK'),
                onPressed: () async{
                    codeDialog = valueText;
                    var userDocumentId = userId; // Replace with the document ID
                    final newItemData = {
                      'status': "Pending",
                      'desc': _textFieldController.text,
                    };
                    FirebaseFirestore.instance
                        .collection('users') // Replace with your outer collection
                        .doc(userDocumentId)
                        .collection('tasks') // Replace with the inner collection
                        .add(newItemData)
                        .then((_) {
                      print('New item added successfully');
                    }).catchError((error) {
                      print('Error adding new item: $error');
                    });
                    Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }


  Future<void> _displayEditInputDialog(BuildContext context, String data,String id,String status) async {
    _textFieldController.text = data;
    _textFieldController.selection =
        TextSelection.fromPosition(
            TextPosition(
                offset:
                _textFieldController
                    .text
                    .length));
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Edit Description'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  valueText = value;
                });
              },
              controller: _textFieldController,
              decoration: const InputDecoration(hintText: "Task description..."),
            ),
            actions: <Widget>[
              MaterialButton(
                color: Colors.green,
                textColor: Colors.white,
                child: const Text('Update'),
                onPressed: () {
                  setState(() {
                    codeDialog = valueText;
                    var userDocumentId = userId;
                    var itemDocumentId = id;// Replace with the document ID
                    final updatedData = {
                      'status': status,
                      'desc': _textFieldController.text,
                    };
                    FirebaseFirestore.instance
                        .collection('users') // Replace with your outer collection
                        .doc(userDocumentId)
                        .collection('tasks') // Replace with the inner collection
                        .doc(itemDocumentId)
                        .update(updatedData)
                        .then((_) {
                      print('New item added successfully');
                    }).catchError((error) {
                      print('Error adding new item: $error');
                    });
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }

}
